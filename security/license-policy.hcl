
# List of SPDX License IDs: https://spdx.org/licenses/

# License SPDX IDs MUST be explicitly listed to pass
allow = ["BSD-3-Clause", "MIT", "Apache-2.0", "MPL-2.0-no-copyleft-exception"]

# Explicitly deny these licenses
deny  = ["MPL-2.0", "GNU General Public License v2.0"]